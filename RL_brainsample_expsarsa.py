
import numpy as np
import pandas as pd


class rlalgorithm:
    def __init__(self, actions, *args, **kwargs):
        """Your code goes here"""
        self.actions = actions  
        self.lr = 0.01
        self.gamma = 0.9
        self.epsilon = 0.1
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Expected Sarsa"

    def choose_action(self, observation):
        self.check_state_exist(observation)
 
        if np.random.uniform() >= self.epsilon:
           
            state_action = self.q_table.loc[observation, :]
           
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
         
            action = np.random.choice(self.actions)

        return action

    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )
            
    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        if s_ != 'terminal':
            a_ = self.choose_action(str(s_))
            # pi (s | a) = 90% 
            # 1/(|A| - 1)
            actions = self.q_table.loc[str(s_), :].index
            
            other_sum = 0
            for each in actions:
                if (each != a_):
                    other_sum += self.lr * 1 / (len(actions) - 1) * self.gamma * self.q_table.loc[s_, each]

            q_target = self.q_table.loc[s,a] + self.lr * (r + 0.9 * self.gamma * self.q_table.loc[s_, a_] - self.q_table.loc[s, a]) + other_sum
        else:
            q_target = r  # next state is terminal
        
        self.q_table.loc[s, a] = q_target  # update
        
        return s_, a_
