
import numpy as np
import pandas as pd


class rlalgorithm:
    def __init__(self, actions, *args, **kwargs):
        self.actions = actions  
        self.lr = 0.01
        self.gamma = 0.9
        self.epsilon = 0.1
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Qlearning"

    def choose_action(self, observation):
        self.check_state_exist(observation)
 
        if np.random.uniform() >= self.epsilon:
           
            state_action = self.q_table.loc[observation, :]
           
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
         
            action = np.random.choice(self.actions)
        return action

    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )
            
    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        if s_ != 'terminal':
            a_ = self.choose_action(str(s_))
            q_target = self.q_table.loc[s,a] + self.lr * (r + self.gamma * self.q_table.loc[s_,].max() - self.q_table.loc[s, a])
        else:
            q_target = r  # next state is terminal
        self.q_table.loc[s, a] = q_target  # update
        return s_,a_
